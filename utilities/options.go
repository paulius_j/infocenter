package utilities

type Options struct {
	Port             int
	Address          string
	TimeoutInSeconds int
}

func CreateOptions(port int, address string, timeoutInSeconds int) *Options {
	options := &Options{
		Port:             port,
		Address:          address,
		TimeoutInSeconds: timeoutInSeconds,
	}

	return options
}
