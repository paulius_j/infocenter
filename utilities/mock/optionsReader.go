package mock

import "../../utilities"

type MockedOptionsReader struct{}

func CreateOptionsReader() *MockedOptionsReader {
	reader := &MockedOptionsReader{}
	return reader
}

func (reader *MockedOptionsReader) Read() *utilities.Options {
	options := utilities.CreateOptions(3000, "/infocenter/", 30)
	return options
}
