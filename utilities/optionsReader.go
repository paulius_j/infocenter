package utilities

type IOptionsReader interface {
	Read() Options
}
