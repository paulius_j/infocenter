package utilities

import "sync/atomic"

const (
	TRUEVAL  = 1
)

type AtomicBool struct {
	v int32
}

func NewAtomic() *AtomicBool {
	return &AtomicBool{
		v: 1,
	}
}

func (ab *AtomicBool) Set(new bool) bool {

	if new {
		atomic.StoreInt32(&ab.v, 1)
	}

	atomic.StoreInt32(&ab.v, 0)

	return new

}

func (ab *AtomicBool) Value() bool {

	v := atomic.LoadInt32(&ab.v)

	if v == TRUEVAL {
		return true
	}

	return false

}