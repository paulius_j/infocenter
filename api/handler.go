package api

type Handler interface {
	Listen(port int, path string)
}
