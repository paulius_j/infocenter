package http

import (
	"../../business/server"
	"../../models"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

type MessageHandler struct {
	ServerCases server.IServerCases
}

func CreateMessageHandler(serverCases server.IServerCases) *MessageHandler {
	handler := &MessageHandler{
		ServerCases: serverCases,
	}

	return handler
}

func (handler *MessageHandler) Listen(port int, path string) {
	addr := ":" + strconv.Itoa(port)
	handler.ServerCases.Start()
	http.Handle(path, handler)
	http.ListenAndServe(addr, nil)
}

func (handler MessageHandler) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	flusher, ok := response.(http.Flusher)

	if !ok {
		http.Error(response, "Streaming unsupported", http.StatusInternalServerError)
		return
	}

	headers := response.Header()
	headers.Set("Access-Control-Allow-Origin", "*")

	switch request.Method {
	case http.MethodGet:
		headers.Set("Content-Type", "text/event-stream")
		headers.Set("Cache-Control", "no-cache")
		headers.Set("Connection", "keep-alive")

		channel := getChannelName(request)
		subscriber := handler.ServerCases.AddListener(channel)

		response.WriteHeader(http.StatusOK)
		flusher.Flush()

		processClosedConnections(request, handler, subscriber)
		sendMessages(response, flusher, subscriber)

	case http.MethodPost:
		channel := getChannelName(request)
		requestBodyText, err := ioutil.ReadAll(request.Body)

		if err != nil {
			response.WriteHeader(http.StatusNoContent)
		} else {
			messageContent := string(requestBodyText[:])
			handler.ServerCases.BroadcastMessage(channel, messageContent)
			response.WriteHeader(http.StatusNoContent)
		}

		flusher.Flush()
	default:
		response.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func processClosedConnections(request *http.Request, handler MessageHandler, subscriber *models.Subscriber) {
	closeNotify := request.Context().Done()

	go func() {
		<-closeNotify
		handler.ServerCases.RemoveListener(subscriber)
	}()
}

func sendMessages(response http.ResponseWriter, flusher http.Flusher, subscriber *models.Subscriber) {
	for message := range subscriber.MessagesToSend {
		fmt.Fprintf(response, message.ToString())
		flusher.Flush()

		if message.Type == models.Timeout {
			return
		}
	}
}

func getChannelName(request *http.Request) string {
	return request.URL.Path
}
