package models

type Subscriber struct {
	channelName    string
	MessagesToSend chan *Message
}

func CreateSubscriber(name string) *Subscriber {
	subscriber := &Subscriber{
		channelName:    name,
		MessagesToSend: make(chan *Message),
	}

	return subscriber
}

func (subscriber *Subscriber) SendMessage(message *Message) {
	if message == nil {
		return
	}

	subscriber.MessagesToSend <- message
}
