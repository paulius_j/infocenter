package models

import (
	"sync"
)

type ConcurrentSubscribersMap struct {
	sync.RWMutex
	items map[*Subscriber]bool
}

type ConcurrentSubscribersMapItem struct {
	Subscriber *Subscriber
	Open       bool
}

func NewConcurrentSubscribersMap() *ConcurrentSubscribersMap {
	cm := &ConcurrentSubscribersMap{
		items: make(map[*Subscriber]bool),
	}

	return cm
}

func (cm *ConcurrentSubscribersMap) Set(key *Subscriber, value bool) {
	cm.Lock()
	defer cm.Unlock()

	cm.items[key] = value
}

func (cm *ConcurrentSubscribersMap) Remove(key *Subscriber) {
	cm.Lock()
	defer cm.Unlock()

	delete(cm.items, key)
}

func (cm *ConcurrentSubscribersMap) Get(key *Subscriber) (bool, bool) {
	cm.Lock()
	defer cm.Unlock()

	value, ok := cm.items[key]

	return value, ok
}

func (cm *ConcurrentSubscribersMap) Iter() <-chan ConcurrentSubscribersMapItem {
	c := make(chan ConcurrentSubscribersMapItem)

	f := func() {
		cm.Lock()
		defer cm.Unlock()

		for k, v := range cm.items {
			c <- ConcurrentSubscribersMapItem{k, v}
		}
		close(c)
	}
	go f()

	return c
}