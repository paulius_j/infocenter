package models

type MessageType string

const (
	Standard MessageType = "msg"
	Timeout  MessageType = "timeout"
)
