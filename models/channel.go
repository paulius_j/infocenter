package models


type Channel struct {
	title       string
	subscribers *ConcurrentSubscribersMap
}

func CreateChannel(title string) *Channel {
	channel := &Channel{
		title:       title,
		subscribers: NewConcurrentSubscribersMap(),
	}

	return channel
}

func (channel *Channel) AddListener(subscriber *Subscriber) {
	if subscriber == nil {
		return
	}

	channel.subscribers.Set(subscriber, true)
}

func (channel *Channel) RemoveListener(subscriber *Subscriber) {
	if subscriber == nil {
		return
	}

	channel.subscribers.Set(subscriber, false)
	channel.subscribers.Remove(subscriber)
}

func (channel *Channel) BroadcastMessage(message *Message) {
	if message == nil {
		return
	}

	for subscriber := range channel.subscribers.Iter() {
		if subscriber.Open {
			subscriber.Subscriber.MessagesToSend <- message
		}
	}
}
