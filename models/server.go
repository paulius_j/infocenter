package models

import (
	"fmt"
	"sync/atomic"
	"time"
)

type Server struct {
	channels      *ConcurrentChannelsMap
	subscribe     chan *Subscriber
	unsubscribe   chan *Subscriber
	timeout       time.Duration
	lastMessageId int32
}

func CreateServer(timeoutInSeconds int) *Server {
	server := &Server{
		channels:    NewConcurrentChannelsMap(),
		subscribe:   make(chan *Subscriber),
		unsubscribe: make(chan *Subscriber),
		timeout:     time.Duration(timeoutInSeconds) * time.Second,
	}

	return server
}

func (server *Server) Run() {
	for {
		select {
		case subscriber := <-server.subscribe:
			channel, exists := server.channels.Get(subscriber.channelName)

			if !exists {
				channel = CreateChannel(subscriber.channelName)
				server.channels.Set(channel.title, channel)
			}

			channel.AddListener(subscriber)
			setTimeout(server, subscriber)
			fmt.Println("Added Listener")

		case subscriber := <-server.unsubscribe:
			channel, exists := server.channels.Get(subscriber.channelName)
			if exists {
				channel.RemoveListener(subscriber)
				if len(channel.subscribers.Iter()) == 0 {
					server.channels.Remove(subscriber.channelName)
				}
				fmt.Println("Removed Listener")
			}
		}
	}
}

func (server *Server) Subscribe(subscriber *Subscriber) {
	if subscriber == nil {
		return
	}

	server.subscribe <- subscriber
}

func (server *Server) Unsubscribe(subscriber *Subscriber) {
	if subscriber == nil {
		return
	}

	server.unsubscribe <- subscriber
}

func (server *Server) Broadcast(channelName string, messageContent string) bool {
	var result bool
	if channel, ok := server.channels.Get(channelName); ok {
		message := Create(server, messageContent, Standard)
		channel.BroadcastMessage(message)
		result = true
	} else {
		result = false
	}

	return result
}

func setTimeout(server *Server, subscriber *Subscriber) {
	go func() {
		time.Sleep(server.timeout)
		server.Unsubscribe(subscriber)
		message := Create(server, server.timeout.String(), Timeout)
		subscriber.SendMessage(message)
	}()
}

func (server *Server) NextMessageId() int {
	atomic.AddInt32(&server.lastMessageId, 1)
	return int(atomic.LoadInt32(&server.lastMessageId))
}