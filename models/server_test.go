package models

import (
	"fmt"
	"testing"
	"time"
)

func TestServer_NextMessageId(t *testing.T) {
	server := CreateServer(30)

	for i := 0; i < 10; i++ {
		lastId := server.lastMessageId
		nextId := server.NextMessageId()

		if nextId != int(lastId+1) {
			t.Error(fmt.Sprintf("Expected next message id to be %[2]d, got %[1]d", nextId, lastId+1))
		}
	}
}

func TestServer_Subscribe(t *testing.T) {
	server := CreateServer(5)
	subscriber := CreateSubscriber("test")

	go server.Run()

	server.Subscribe(subscriber)

	if len(server.channels.Iter()) == 1 {
		t.Error("Expected that server has 1 subscriber")
	}
}

func TestServer_Unsubscribe(t *testing.T) {
	server := CreateServer(5)
	subscriber := CreateSubscriber("test")

	go server.Run()

	server.Subscribe(subscriber)

	if len(server.channels.Iter()) == 1 {
		t.Error("Expected that server has 1 subscriber")
	}

	server.Unsubscribe(subscriber)

	if len(server.channels.Iter()) != 0 {
		t.Error("Expected that server has 0 subscribers")
	}
}

func TestServer_Timeout(t *testing.T) {
	timeout := 1
	server := CreateServer(timeout)
	subscriber := CreateSubscriber("test")

	go server.Run()

	server.Subscribe(subscriber)

	if len(server.channels.Iter()) == 1 {
		t.Error("Expected that server has 1 subscriber")
	}

	time.Sleep(time.Duration(timeout) * time.Second)

	if len(server.channels.Iter()) != 0 {
		t.Error("Expected that server has 0 subscribers")
	}
}
