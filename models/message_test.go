package models

import (
	"testing"
)

func TestCreateMessage_WithType_Timeout(t *testing.T) {
	timeoutMessage := CreateMessage("1", "test", Timeout)

	if timeoutMessage.LastEventId != "1" {
		t.Error("Expected LastEventId to be 1")
	}

	if timeoutMessage.Data != "test" {
		t.Error("Expected Data to be test")
	}

	if timeoutMessage.Type != Timeout {
		t.Error("Expected message type to be Timeout")
	}
}

func TestCreateMessage_WithType_Standard(t *testing.T) {
	standardMessage := CreateMessage("1", "test", Standard)

	if standardMessage.LastEventId != "1" {
		t.Error("Expected LastEventId to be 1")
	}

	if standardMessage.Data != "test" {
		t.Error("Expected Data to be test")
	}

	if standardMessage.Type != Standard {
		t.Error("Expected message type to be Timeout")
	}
}

func TestToString_WithType_Standard(t *testing.T) {
	standardMessage := CreateMessage("1", "test", Standard)
	textRepresentation := standardMessage.ToString()

	if textRepresentation != "id: 1\nevent: msg\ndata: test\n\n" {
		t.Error("Standard message's string representation is incorrect")
	}
}

func TestToString_WithType_Timeout(t *testing.T) {
	timeoutMessage := CreateMessage("1", "test", Timeout)
	textRepresentation := timeoutMessage.ToString()

	if textRepresentation != "event: timeout\ndata: test\n\n" {
		t.Error("Standard message's string representation is incorrect")
	}
}