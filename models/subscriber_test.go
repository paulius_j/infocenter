package models

import (
	"testing"
)

func TestCreateSubscriber(t *testing.T) {
	subscriber := CreateSubscriber("test")

	if subscriber.channelName != "test" {
		t.Error("Expected channel name to be test")
	}

	if subscriber.MessagesToSend == nil {
		t.Error("Expected messages to send to not be null")
	}
}
