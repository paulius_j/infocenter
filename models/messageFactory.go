package models

import "strconv"

func Create(server *Server, content string, msgType MessageType) *Message {
	var messageId string;

	if msgType != Timeout {
		messageId = strconv.Itoa(server.NextMessageId())
	}

	message := CreateMessage(messageId, content, msgType)
	return message
}