package models

import (
	"sync"
)

type ConcurrentChannelsMap struct {
	sync.RWMutex
	items map[string]*Channel
}

type ConcurrentChannelsMapItem struct {
	Name 		string
	Channel     *Channel
}

func NewConcurrentChannelsMap() *ConcurrentChannelsMap {
	cm := &ConcurrentChannelsMap{
		items: make(map[string]*Channel),
	}

	return cm
}

func (cm *ConcurrentChannelsMap) Set(key string, value *Channel) {
	cm.Lock()
	defer cm.Unlock()

	cm.items[key] = value
}

func (cm *ConcurrentChannelsMap) Remove(key string) {
	cm.Lock()
	defer cm.Unlock()

	delete(cm.items, key)
}

func (cm *ConcurrentChannelsMap) Get(key string) (*Channel, bool) {
	cm.Lock()
	defer cm.Unlock()

	value, ok := cm.items[key]

	return value, ok
}

func (cm *ConcurrentChannelsMap) Iter() <-chan ConcurrentChannelsMapItem {
	c := make(chan ConcurrentChannelsMapItem)

	f := func() {
		cm.Lock()
		defer cm.Unlock()

		for k, v := range cm.items {
			c <- ConcurrentChannelsMapItem{k, v}
		}
		close(c)
	}
	go f()

	return c
}