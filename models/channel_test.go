package models

import "testing"

func TestCreateChannel(t *testing.T) {
	channel := CreateChannel("test")

	if channel.title != "test" {
		t.Error("Expected channel name to be test")
	}

	if channel.subscribers == nil {
		t.Error("Expected subscribers to not be null")
	}

	if len(channel.subscribers.Iter()) != 0 {
		t.Error("Expected subscribers to be empty")
	}
}

func TestChannel_AddListener(t *testing.T) {
	channel := CreateChannel("test")
	subscriber := CreateSubscriber("test")
	channel.AddListener(subscriber)

	if len(channel.subscribers.Iter()) == 1 {
		t.Error("Expected channel to have 1 subscriber")
	}
}

func TestChannel_RemoveListener(t *testing.T) {
	channel := CreateChannel("test")
	subscriber := CreateSubscriber("test")
	channel.AddListener(subscriber)

	if len(channel.subscribers.Iter()) == 1 {
		t.Error("Expected channel to have 1 subscriber")
	}

	channel.RemoveListener(subscriber)

	if len(channel.subscribers.Iter()) != 0 {
		t.Error("Expected subscribers to be empty")
	}
}
