package models

import (
	"bytes"
	"fmt"
	"strings"
)

type Message struct {
	LastEventId string
	Data        string
	Type        MessageType
}

func CreateMessage(id string, data string, msgType MessageType) *Message {
	message := &Message{
		LastEventId: id,
		Data:        data,
		Type:        msgType,
	}

	return message
}

func (message *Message) ToString() string {
	var buffer bytes.Buffer

	if message.Type != Timeout {
		buffer.WriteString(fmt.Sprintf("id: %s\n", message.LastEventId))
	}
	buffer.WriteString(fmt.Sprintf("event: %s\n", message.Type))
	buffer.WriteString(fmt.Sprintf("data: %s\n", strings.Replace(message.Data, "\n", "\ndata: ", -1)))
	buffer.WriteString("\n")

	return buffer.String()
}
