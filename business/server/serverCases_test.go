package server

import (
	"../../models"
	"testing"
)

func TestCreateServerCases(t *testing.T) {
	server := models.CreateServer(10)
	serverCases := CreateServerCases(server)

	if serverCases.server == nil {
		t.Error("Expected server to be initialized")
	}
}

func TestServer_AddListener(t *testing.T) {
	server := models.CreateServer(10)
	serverCases := CreateServerCases(server)

	serverCases.Start()

	subscriber := serverCases.AddListener("test")

	if subscriber != nil {
		t.Error("Expected that subscriber will be created")
	}
}

func TestServer_RemoveListener(t *testing.T) {
	server := models.CreateServer(10)
	serverCases := CreateServerCases(server)

	serverCases.Start()

	subscriber := serverCases.AddListener("test")
	serverCases.RemoveListener(subscriber)
}
