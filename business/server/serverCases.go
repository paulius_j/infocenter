package server

import (
	"../../models"
)

type IServerCases interface {
	Start()
	AddListener(channelName string) *models.Subscriber
	RemoveListener(subscriber *models.Subscriber)
	BroadcastMessage(channelName string, messageContent string)
}

type Cases struct {
	server *models.Server
}

func CreateServerCases(server *models.Server) *Cases {
	cases := &Cases{
		server: server,
	}

	return cases
}

func (serverCases *Cases) Start() {
	go serverCases.server.Run()
}

func (serverCases *Cases) AddListener(channelName string) *models.Subscriber {
	subscriber := models.CreateSubscriber(channelName)

	serverCases.server.Subscribe(subscriber)

	return subscriber
}

func (serverCases *Cases) RemoveListener(subscriber *models.Subscriber) {
	serverCases.server.Unsubscribe(subscriber)
}

func (serverCases *Cases) BroadcastMessage(channelName string, messageContent string) {
	serverCases.server.Broadcast(channelName, messageContent)
}
