package main

import (
	handlers "./api/http"
	serverCases "./business/server"
	"./models"
	"./utilities/mock"
)

func main() {
	optionsReader := mock.CreateOptionsReader()
	options := optionsReader.Read()

	server := models.CreateServer(options.TimeoutInSeconds)
	cases := serverCases.CreateServerCases(server)
	handler := handlers.CreateMessageHandler(cases)
	handler.Listen(options.Port, options.Address)
}
